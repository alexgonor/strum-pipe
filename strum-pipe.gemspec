# frozen_string_literal: true

require_relative "lib/strum/pipe/version"

Gem::Specification.new do |spec|
  spec.name          = "strum-pipe"
  spec.version       = Strum::Pipe::VERSION
  spec.authors       = ["Serhiy Nazarov"]
  spec.email         = ["sn@nazarov.com.ua"]

  spec.summary       = "Light ruby framework"
  spec.description   = "Strum Pipe"
  spec.homepage      = "https://gitlab.com/strum-rb/strum-pipe"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/strum-rb/strum-pipe"
  spec.metadata["changelog_uri"] = "https://gitlab.com/strum-rb/strum-pipe/-/blob/master/CHANGELOG.md"
  spec.metadata["bug_tracker_uri"] = "https://gitlab.com/strum-rb/strum-pipe/issues"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "strum-service", "~> 0.1"
end
